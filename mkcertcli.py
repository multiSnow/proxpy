#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from argparse import ArgumentParser
from argparse import ArgumentDefaultsHelpFormatter as _HelpFormatter

from mkcert import mkcert,default_params

class ArgumentDefaultsHelpFormatter(_HelpFormatter):
    def _get_help_string(self, action):
        default=action.default
        choices=action.choices
        if not default:
            return action.help
        if choices:
            action.help='{} (choose from {})'.format(
                action.help,
                ', '.join(f"'{choice}'" for choice in sorted(choices)))
        return super()._get_help_string(action)

def main():
    parser=ArgumentParser(prog='mkcertcli',
                          formatter_class=ArgumentDefaultsHelpFormatter)

    for flag,kwds in default_params.items():
        parser.add_argument(flag,**kwds)

    params=parser.parse_args()
    if params.ca:
        mkcert(params)
    else:
        print('do nothing')

if __name__=='__main__':
    main()

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
