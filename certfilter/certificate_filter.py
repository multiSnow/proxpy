#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from re import compile as recpl

from ._module_dir import MODULE_DIR

RULES=MODULE_DIR/'rules'

RULES_TABLE={'rules':[],'ts':0}

ruleptn=recpl(r'(?P<side>[A-Za-z]+)\s+(?P<field>[A-Za-z]+)\s+(?P<regex>.+)')

def certfilter(cert):
    '''Certificate filter
    cert is a instance of http0._cert.Cert
    if this function return True, the certificate will be refused.
    '''
    if not RULES.exists():
        return False
    if (ts:=RULES.stat().st_mtime_ns)!=RULES_TABLE['ts']:
        RULES_TABLE['rules'].clear()
        RULES_TABLE['ts']=ts
        for line in filter(None,RULES.read_text().splitlines()):
            if line.startswith('#'):
                continue
            if (m:=ruleptn.fullmatch(line)) is None:
                raise ValueError(f'invalid line: {line}')
            if (side:=m['side']) not in ('issuer','subject','subjectAltName'):
                raise ValueError(f'invalid side of line: {line}')
            if (field:=m['field'].lower()) not in ('c','st','l','o','ou','cn'):
                raise ValueError(f'invalid field of line: {line}')
            RULES_TABLE['rules'].append((side,field,recpl(m['regex'])))
    for side,field,pattern in RULES_TABLE['rules']:
        for s in getattr(getattr(cert,side),field):
            if pattern.search(s):
                return True
    return False

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
