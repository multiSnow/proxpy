#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path

MODULE_DIR=Path(__file__).resolve().parent

def getpysrc():
    srcbuf=[]
    lines=[]
    for py in MODULE_DIR.glob('*.py'):
        lines.append(py.relative_to(MODULE_DIR.parent).as_posix())
        lines.append('\n\n')
        with py.open(mode='rt',encoding='utf8') as f:
            for line in f:
                lines.append(f'## {line}')
        srcbuf.append(''.join(lines))
        lines.clear()
    srcbuf.sort()
    return '\n\n'.join(srcbuf)

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
