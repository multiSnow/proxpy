#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from _thread import allocate_lock,start_new_thread
from argparse import ArgumentParser
from collections import OrderedDict
from http import HTTPStatus
from http.client import HTTP_PORT,HTTPS_PORT
from http.server import BaseHTTPRequestHandler
from os import urandom
from pathlib import Path
from ssl import SSLContext,PROTOCOL_TLS_SERVER,create_default_context
from socketserver import ThreadingTCPServer,StreamRequestHandler
from tempfile import TemporaryDirectory
from threading import Barrier
from time import gmtime,strftime,time
from urllib.parse import urlparse as _urlparse

from mkcert import mkcert,mkparams
from dohjson import DNSClient
from recert import recert
from redirector import redirect_url
import crypto

from croxy import CryptoConnection as _CryptoConnection
from croxy import CacheCryptoConnection as _CacheCryptoConnection
from http0 import HTTPConnection as _HTTPConnection
from http0 import LocalCacheConnection as _LocalCacheConnection

from proxpycfg import prompt_loadconf,nonedict

try:
    from certfilter import certfilter
except:
    certfilter=None

# maximal line length when calling readline().
_MAXLINE=65536

class ABC:
    def __init__(self,**kwds):
        for k,v in kwds.items():
            setattr(self,k,v)

class IDPOOL:
    def __init__(self,length=4):
        self._pool=set()
        self._lock=allocate_lock()
        self._len=length

    def newid(self):
        with self._lock:
            while (s:=urandom(self._len).hex()) in self._pool:
                pass
            self._pool.add(s)
            return s

    def delid(self,s):
        with self._lock:
            try:
                self._pool.remove(s)
            except:
                pass

PROXYENV=ABC(
    ROOT=Path(__file__).resolve().parent,
    CACERT=None,
    DATADIR=None,
    SERVER_NAME='Proxpy',
    TLSCTX={},
    LOCK=allocate_lock(),
    IDP=IDPOOL()
)

MKCERT_ENV=nonedict()
REMOTE_ENV=nonedict()

def now():
    return strftime('%FT%TZ',gmtime(time()))

def log(msg,/,uid=None):
    if uid:
        return print(f'{now()}({uid}) {msg}')
    return print(f'{now()} {msg}')

def new_tls_context(cafile=None,cadata=None):
    if cafile is None and cadata is None:
        cafile=PROXYENV.ROOT/PROXYENV.CACERT
    ctx=create_default_context(cafile=cafile,cadata=cadata)
    ctx.set_alpn_protocols(['http/1.1'])
    ctx.post_handshake_auth=True
    return ctx

class DirectConnection(_HTTPConnection):
    def __init__(self,hostname,port=None,scheme=None,**kwds):
        if scheme=='https':
            matchname,tlsname,_,cert=recert(hostname,REMOTE_ENV['prikey'])
            if matchname is not None:
                if tlsname is not None:
                    kwds['hostname']=hostname
                    kwds['tlsname']=tlsname
                if cert is not None:
                    kwds['context']=new_tls_context(cadata=cert)
            if kwds.get('context',None) is None:
                kwds['context']=new_tls_context()
        super().__init__(hostname,port=port,certfilter=certfilter,**kwds)

class CachedConnection(_LocalCacheConnection):
    def __init__(self,hostname,port=None,scheme=None,**kwds):
        if scheme=='https':
            matchname,tlsname,_,cert=recert(hostname,REMOTE_ENV['prikey'])
            if matchname is not None:
                if tlsname is not None:
                    kwds['hostname']=hostname
                    kwds['tlsname']=tlsname
                if cert is not None:
                    kwds['context']=new_tls_context(cadata=cert)
            if kwds.get('context',None) is None:
                kwds['context']=new_tls_context()
        super().__init__(hostname,port=port,cachedir=PROXYENV.DATADIR,
                         certfilter=certfilter,logger=log,**kwds)

class NoSNIConnection(DirectConnection):
    def __init__(self,*args,**kwds):
        super().__init__(*args,sni=False,**kwds)

class CachedNoSNIConnection(CachedConnection):
    def __init__(self,*args,**kwds):
        super().__init__(*args,sni=False,**kwds)

class CryptoConnection(_CryptoConnection):
    def __init__(self,hostname,port=None,scheme=None,**kwds):
        ctx=None
        if REMOTE_ENV['https']:
            ctx=new_tls_context()
        if scheme=='https':
            matchname,tlsname,_,cert=recert(hostname,REMOTE_ENV['prikey'])
            if matchname is not None:
                if tlsname is not None:
                    kwds['hostname']=hostname
                    kwds['tlsname']=tlsname
                if cert is not None:
                    kwds['context']=new_tls_context(cadata=cert)
            if kwds.get('context',None) is None:
                kwds['context']=new_tls_context()
        super().__init__(hostname,port=port,certfilter=certfilter,**kwds)
        self.setremote(
            REMOTE_ENV['ip'],port=REMOTE_ENV['port'],path=REMOTE_ENV['path'],
            hostname=REMOTE_ENV['name'],tlsname=REMOTE_ENV['name'],
            context=ctx,sni=REMOTE_ENV['sni'],pubkey=REMOTE_ENV['pubkey'],
            client_prikey=REMOTE_ENV['prikey'],signature=REMOTE_ENV['signature'])

class CacheCryptoConnection(_CacheCryptoConnection):
    def __init__(self,hostname,port=None,scheme=None,**kwds):
        ctx=None
        if REMOTE_ENV['https']:
            ctx=new_tls_context()
        if scheme=='https':
            matchname,tlsname,_,cert=recert(hostname,REMOTE_ENV['prikey'])
            if matchname is not None:
                if tlsname is not None:
                    kwds['hostname']=hostname
                    kwds['tlsname']=tlsname
                if cert is not None:
                    kwds['context']=new_tls_context(cadata=cert)
            if kwds.get('context',None) is None:
                kwds['context']=new_tls_context()
        kwds['cachedir']=REMOTE_ENV['cache']
        super().__init__(hostname,port=port,certfilter=certfilter,
                         logger=log,**kwds)
        self.setremote(
            REMOTE_ENV['ip'],port=REMOTE_ENV['port'],path=REMOTE_ENV['path'],
            hostname=REMOTE_ENV['name'],tlsname=REMOTE_ENV['name'],
            context=ctx,sni=REMOTE_ENV['sni'],pubkey=REMOTE_ENV['pubkey'],
            client_prikey=REMOTE_ENV['prikey'],signature=REMOTE_ENV['signature'])

BACKEND={
    'direct':DirectConnection,
    'cached':CachedConnection,
    'nosni':NoSNIConnection,
    'cnosni':CachedNoSNIConnection,
    'crypto':CryptoConnection,
    'ccrypto':CacheCryptoConnection,
}

def urlparse(url):
    return _urlparse(url,allow_fragments=False)

def certpair(serv,tempdir=None):
    if not serv:
        raise ValueError('server name is empty.')
    params=mkparams(update={
        'serv':serv,
        'servdir':tempdir,
        'dnslist':[serv],
        **MKCERT_ENV})
    return mkcert(params)

def wrap_tls_server(s,host,uid=None):
    with PROXYENV.LOCK:
        if host in PROXYENV.TLSCTX:
            if time()>PROXYENV.TLSCTX[host].expire:
                log(f'mkcert: discard cert of {host}',uid)
                del PROXYENV.TLSCTX[host]
        if host not in PROXYENV.TLSCTX:
            with TemporaryDirectory(prefix='proxpy-site-certs-') as _tmpdir:
                crt,key=certpair(host,tempdir=Path(_tmpdir).resolve())
                PROXYENV.TLSCTX[host]=ABC(
                    context=SSLContext(protocol=PROTOCOL_TLS_SERVER),
                    expire=time()+MKCERT_ENV['servdays']*24*60*60,
                )
                PROXYENV.TLSCTX[host].context.load_cert_chain(certfile=crt,keyfile=key)
            log(f'mkcert: new proxy cert for {host}',uid)
    try:
        return PROXYENV.TLSCTX[host].context.wrap_socket(s,server_side=True)
    except Exception as e:
        return log(f'mkcert: ssl connect failed: {host} {e}',uid)

class ProxyHandler(BaseHTTPRequestHandler):
    protocol_version='HTTP/1.1'
    supported_method=('CONNECT','GET','POST','HEAD','OPTIONS','PUT','PATCH','DELETE')
    default_scheme_port={'http':HTTP_PORT,'https':HTTPS_PORT}

    def __init__(self,*args,**kwds):
        self.lock=allocate_lock()
        self.destsrv=None
        self.parsed_url=urlparse('http:/')
        self.request_url=None
        self.response_override=[]
        self.uid=PROXYENV.IDP.newid()
        self.handle_count=0
        self.handle_noreq_count=0
        return super().__init__(*args,**kwds)

    def handle(self):
        log(f'{self.server.name}: new handler',self.uid)
        self.upstream=None
        self.upstream_backend=None
        self.upstream_target=None
        super().handle()
        if self.upstream is not None:
            upstream=self.upstream
            self.upstream=None
            self.upstream_backend=None
            self.upstream_target=None
            upstream.close()
        hostname=self.parsed_url.hostname
        url=f': {self.parsed_url.scheme}:{hostname}' if hostname else ''
        count=f'(handle {self.handle_count}'
        if self.handle_noreq_count:
            count=f'{count} with {self.handle_noreq_count} no request)'
        else:
            count=f'{count})'
        log(f'{self.server.name}: close handler{url} {count}',
            self.uid)
        PROXYENV.IDP.delid(self.uid)

    def handle_one_request(self):
        with self.lock:
            url=''
            if self.parsed_url.hostname:
                url=f': {self.parsed_url.scheme}:{self.parsed_url.hostname}'
            log(f'{self.server.name}: handler start{url}',self.uid)
            super().handle_one_request()
            if self.parsed_url.hostname:
                url=f'{self.parsed_url.scheme}:{self.parsed_url.hostname}'
                self.handle_count+=1
            else:
                url='no request'
                self.handle_noreq_count+=1
            log(f'{self.server.name}: handler end: {url}',self.uid)

    def version_string(self):
        return PROXYENV.SERVER_NAME

    def send_response(self,code,message=None):
        self.log_request(code)
        self.send_response_only(code,message)

    def log_request(self,code='-',size='-'):
        if self.command=='CONNECT':return
        return #super().log_request(code=code,size=size)

    def receive(self,length,blocksize=1024):
        while length>0 and not self.rfile.closed:
            yield (b:=self.rfile.read(min(length,blocksize)))
            length-=len(b)

    def receive_chunked(self):
        while size:=self._next_chunk_size():
            yield self.rfile.read(size)
            if self.rfile.readline().rstrip(b'\r\n'):
                raise Exception('chunk data error')

    def _next_chunk_size(self):
        sizeline=self.rfile.readline(_MAXLINE+1)
        if len(sizeline)>_MAXLINE:
            raise Exception('chunk size too long')
        extensions=sizeline.rstrip(b'\r\n').split(b';')
        try:
            size=int(extensions.pop(0),base=16)
        except ValueError:
            self.close()
            raise Exception('unknown byte in chunk size')
        return size

    def do_before(self,post=False):
        # receive/parse the request of client
        if post:
            if self.headers.get('transfer-encoding','').lower()=='chunked':
                receive_iter=self.receive_chunked()
            elif 'content-length' in self.headers:
                receive_iter=self.receive(int(self.headers.get('content-length')))
            elif self.command=='DELETE':
                # assume that DELETE without content-length header has no request body
                receive_iter=None
            else:
                # refuse other post-like methods without transfer-encoding and content-length
                self.close_connection=True
                self.send_error(HTTPStatus.LENGTH_REQUIRED)
                return
        else:
            receive_iter=None
        if 'proxy-connection' in self.headers:
            del self.headers['proxy-connection']
        # fix irregular url path
        while self.path.startswith('//'):
            self.path=self.path[1:]
        urlres=urlparse(self.path)
        port=urlres.port or self.parsed_url.port
        netloc=urlres.hostname or self.parsed_url.hostname
        if port and port!=self.default_scheme_port[self.parsed_url.scheme]:
            netloc=f'{netloc}:{port}'
        if self.parsed_url.scheme!='https':
            self.parsed_url=self.parsed_url._replace(netloc=netloc)
        self.parsed_url=self.parsed_url._replace(
            netloc=netloc,path=urlres.path,
            params=urlres.params,query=urlres.query)

        # redirector
        request_url=self.parsed_url.geturl()
        redirected_url,is_implicit,ruleset_name,backend,*override=redirect_url(
            request_url,pubkey=self.server.pubkey)
        request_override,response_override=override
        self.response_override[:]=response_override
        if redirected_url is None:
            # no rule matched
            self.upstream_target=self.parsed_url
        else:

            if backend=='filtered':
                # filtered backend, keep connection opening with empty content.
                log(f'{self.server.name}: filtered: {request_url}',self.uid)
                status=200
                response_headers={
                    'content-length':'0',
                    'content-type':'text/plain',
                }
                try:
                    self.start_response(status,response_headers)
                    self.send_data(b'')
                except:
                    # connection closed
                    self.close_connection=True
                return

            msg='implicit' if is_implicit else 'explicit'
            msg=f'{self.server.name}: redirect: ({ruleset_name} {msg})'
            msg=f'{msg} {self.command} {request_url}'
            if request_url!=redirected_url:
                msg=f'{msg} => {redirected_url}'
            log(msg,self.uid)
            parsed_url=urlparse(redirected_url)
            if is_implicit:
                # implicit redirect, create upstream according to redirected_url
                self.upstream_target=parsed_url
            else:
                # explicit redirect, send 308
                status=308
                response_headers={
                    'location':redirected_url,
                    'content-length':'0',
                    'content-type':'text/plain',
                }
                if parsed_url[:2]!=self.parsed_url[:2]:
                    # scheme or netloc changed, connection close
                    response_headers['connection']='close'
                    self.close_connection=True
                try:
                    self.start_response(status,response_headers)
                    self.send_data(b'')
                except:
                    # connection closed
                    self.close_connection=True
                return

        if backend is None:
            # no redirect or not changed in redirect, use self.server.backend
            backend=self.server.backend
        elif backend not in BACKEND:
            # invalid backend, show in log and close connection with 500 error
            log(f'{self.server.name}: invalid backend: {backend} '
                f'(in {ruleset_name})',self.uid)
            self.close_connection=True
            self.send_error(HTTPStatus.INTERNAL_SERVER_ERROR,
                            explain=f'invalid backend: {backend} (in {ruleset_name})')
            return

        # scheme or netloc or backand changed, close old upstream
        if self.upstream is not None:
            if self.parsed_url[:2]!=self.upstream_target[:2] \
               or backend!=self.upstream_backend:
                upstream=self.upstream
                self.upstream=None
                self.upstream_backend=None
                upstream.close()

        # create upstream and request
        method=self.command
        scheme=self.upstream_target.scheme
        hostname=self.upstream_target.hostname
        port=self.upstream_target.port
        path=self.upstream_target.path
        query=self.upstream_target.query
        headers=OrderedDict((k.lower(),v) for k,v in self.headers.items())

        # websocket is not supported
        # NOTE: filterout websocket just before upstream created,
        #       so user could redirect websocket url
        if headers.get('upgrade','').lower()=='websocket':
            log(f'{self.server.name}: unsupported websocket request: '
                f'{method} {scheme}:{hostname}{path}',self.uid)
            self.close_connection=True
            msg=b'websocket is not supported.'
            try:
                self.start_response(404,{
                    'content-length':f'{len(msg)}',
                    'content-type':'text/plain',
                })
                self.send_data(msg)
            except:
                pass
            return

        # apply request header override
        for key,value in request_override:
            if value is None:
                headers.pop(key,None)
            else:
                headers[key]=value

        if self.upstream is None:
            self.upstream_backend=backend
            log(f'{self.server.name}: new upstream ({self.upstream_backend}): '
                f'{scheme}:{hostname}',self.uid)
            self.upstream=BACKEND[self.upstream_backend](
                hostname,port=port,scheme=scheme,uid=self.uid,
                dnsclient=self.server.dnsclient)
        log(f'{self.server.name} ({self.upstream_backend}): request: '
            f'{method} {scheme}:{hostname}{path}',self.uid)
        try:
            self.upstream.request(
                method,f'{path}?{query}' if query else path,
                body=receive_iter,
                headers=headers)
        except Exception as e:
            log(f'{self.server.name} ({self.upstream_backend}): request failed: '
                f'{method} {scheme}:{hostname}{path} ({e})',self.uid)
            self.close_connection=True
            return
        else:
            return self.upstream

    def do_after(self):
        scheme=self.upstream_target.scheme
        hostname=self.upstream_target.hostname
        path=self.upstream_target.path
        internal_err=False
        # receive/parse the response of upstream and send response to client
        status=self.upstream.status
        response_headers=self.upstream.response_headers.copy()
        is_chunked=response_headers.get('transfer-encoding','').lower()=='chunked'

        # apply response header override
        for key,value in self.response_override:
            if value is None:
                response_headers.pop(key,None)
            else:
                response_headers[key]=value
        self.response_override.clear()

        if status>599:
            status-=100
            internal_err=True
        else:
            log(f'{self.server.name} ({self.upstream_backend}): response ({status}): '
                f'{self.command} {scheme}:{hostname}{path}',self.uid)
        if status>499:
            response_headers['connection']='close'
        if status>299:
            location=response_headers.get('location',None)
            if location is not None:
                # close if redirect to another site
                other=urlparse(location)
                if other[:2]!=self.upstream_target[:2]:
                    response_headers['connection']='close'
        if response_headers.get('connection','').lower()=='close':
            self.close_connection=True

        try:
            self.start_response(status,response_headers)
            if internal_err:
                data=b''.join(self.upstream.response_iter())
                msg=data.decode('utf8')
                if is_chunked:
                    self.send_chunk(data)
                    self.send_chunk()
                else:
                    self.send_data(data)
                log(f'{self.server.name} ({self.upstream_backend}): '
                    f'response ({status+100} {msg}): '
                    f'{scheme}:{hostname}{path}',self.uid)
            elif is_chunked:
                for block in self.upstream.response_iter():
                    self.send_chunk(block)
                self.send_chunk()
            else:
                for block in self.upstream.response_iter():
                    self.send_data(block)
        except BrokenPipeError:
            # client closed.
            self.close_connection=True
        except Exception as e:
            self.close_connection=True
            log(f'{self.server.name} ({self.upstream_backend}): response interrupt: '
                f'{self.command} {scheme}:{hostname}{path} ({e})',self.uid)

    def start_response(self,status,headers):
        self.send_response(status)
        for k,v in headers.items():
            if isinstance(v,str):
                for line in v.splitlines():
                    self.send_header(k,line)
            else:
                self.send_header(k,v)
        self.end_headers()

    def send_data(self,data):
        self.wfile.write(data)
        self.wfile.flush()

    def send_chunk(self,data=b''):
        self.send_data(f'{len(data):x}'.encode('utf8'))
        self.send_data(b'\r\n')
        self.send_data(data)
        self.send_data(b'\r\n')

    def do_CONNECT(self):
        self.parsed_url=self.parsed_url._replace(scheme='https',netloc=self.path)
        host=self.parsed_url.hostname
        log(f'{self.server.name}: client connect: '
            f'{self.parsed_url.scheme}:{host}',self.uid)
        self.destsrv=host
        self.start_response(HTTPStatus.OK,{})
        self.request=wrap_tls_server(self.request,host,uid=self.uid)
        if self.request is None:
            self.close_connection=True
            return
        self.setup()
        conntype=self.headers.get('proxy-connection','keep-alive').lower()
        self.close_connection=conntype!='keep-alive'

    def do_main(self,post=False):
        if self.do_before(post=post) is None:
            return
        if self.upstream is None:return
        self.do_after()

    def do_GET(self):
        return self.do_main()

    def do_HEAD(self):
        # treat HEAD same as GET
        return self.do_GET()

    def do_OPTIONS(self):
        # treat OPTIONS same as HEAD
        return self.do_HEAD()

    def do_POST(self):
        return self.do_main(post=True)

    def do_PUT(self):
        # treat PUT same as POST
        return self.do_POST()

    def do_PATCH(self):
        # treat PATCH same as POST
        return self.do_POST()

    def do_DELETE(self):
        # treat DELETE same as POST
        return self.do_POST()

class ProxyServer(ThreadingTCPServer):
    def __init__(self,servaddr,handler,dnsclient,backend,name=None,pubkey=None):
        super().__init__(servaddr,handler)
        self.dnsclient=dnsclient
        self.backend=backend
        self.name=name
        self.pubkey=pubkey

def serve_proxy(server,barrier):
    with server:
        server.serve_forever()
    log(f'{server.name} end')
    barrier.wait()

def startproxy(name=None,server=None,pubkey=None,dns=None,barrier=None,pool={}):
    if name in pool:
        raise ValueError(f'server name duplicate: {name}')
    backend,ip,port=server
    for existing_name,existing_server in pool.items():
        if existing_server.server_address==(ip,port):
            raise ValueError(f'server address duplicate: {ip}:{port}')
    pool[name]=ProxyServer((ip,port),ProxyHandler,dns,backend,name=name,pubkey=pubkey)
    log(f'run {name} on {ip}:{port}')
    start_new_thread(serve_proxy,(pool[name],barrier))

def main():
    parser=ArgumentParser(prog='proxpy')
    parser.add_argument(
        '-P',dest='passwd',action='store',metavar='PASSWORD',
        help='password to load config file.')
    if (conf:=prompt_loadconf(parser.parse_args().passwd)) is None:
        return 1
    if not conf['proxy']['servers']:
        return log('no server exists.') or 1

    serverpool={}
    lock=allocate_lock()
    barrier=Barrier(len(conf['proxy']['servers'])+1)

    REMOTE_ENV['prikey']=conf['prikey']
    REMOTE_ENV.update(conf['remote'])
    MKCERT_ENV.update(
        ca=conf['proxy']['ca_name'],
        cadir=PROXYENV.ROOT,
        keyalg=conf['proxy']['keyalg'],
        curve=conf['proxy']['curve'],
        md=conf['proxy']['md'],
        cadays=conf['proxy']['ca_expire'],
        servdays=conf['proxy']['site_expire'],
        overwrite_ca=False,
        overwrite_serv=False,
    )

    PROXYENV.CACERT=conf['cacert']
    PROXYENV.DATADIR=conf['cache']

    pubkey=crypto.generate_pubkey(conf['prikey'])
    dnsclient_kwds=dict(
        dns_ip=conf['doh']['ip'],
        dns_port=conf['doh']['port'],
        dns_name=conf['doh']['name'],
        dns_path=conf['doh']['path'],
        cacert=conf['doh']['cacert'],
    )

    with DNSClient(**dnsclient_kwds) as dns:
        with lock:
            for name,server in conf['proxy']['servers'].items():
                startproxy(name=name,server=server,pubkey=pubkey,
                           dns=dns,barrier=barrier,pool=serverpool)
            try:
                while True:
                    lock.acquire(timeout=1)
            except KeyboardInterrupt:
                pass
        log(f'interrupt {PROXYENV.SERVER_NAME}')
        for server in serverpool.values():
            start_new_thread(server.shutdown,())
        barrier.wait()
    log(f'{PROXYENV.SERVER_NAME} exit')

if __name__=='__main__':
    exit(main())

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
