#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from argparse import ArgumentParser,Action

def show(recerts):
    if not recerts['rules']:
        return print('no recert rules.')
    buf=[('matchname','tlsname','certificate fingerprint')]
    for hostname,(tlsname,fingerprint) in recerts['rules'].items():
        if tlsname=='':
            tlsname='(disabled)'
        elif tlsname is None:
            tlsname='(no override)'
        if fingerprint is None:
            fingerprint='(no override)'
        buf.append((hostname,tlsname,fingerprint))
    fmt='  '.join(f'{{:>{max(map(len,col))}}}' for col in zip(*buf))
    for line in buf:
        print(fmt.format(*line))

def get_cert_fingerprint(data):
    # lazy import
    from mkcert import openssl_noout
    line=openssl_noout('x509','-noout','-fingerprint','-sha256',stdin=data)
    line=line.rstrip().removeprefix(b'sha256 Fingerprint=').translate(None,b':')
    return line.decode('utf8')

def clean_cert(fingerprint,recerts):
    if fingerprint not in recerts['certs']:return
    if fingerprint not in set(f for _,f in recerts['rules'].values()):
        del recerts['certs'][fingerprint]

def minimize_certificate(cert):
    in_cert=False
    buf=[]
    for line in cert.splitlines():
        line=line.strip()
        if not line:
            continue
        if line.startswith('-----BEGIN '):
            in_cert=True
            buf.append(line)
        elif line.startswith('-----END '):
            in_cert=False
            buf.append(line)
        elif in_cert==True:
            buf.append(line)
    buf.append('')
    return '\n'.join(buf)

def run(ns):

    # lazy import
    import crypto
    from proxpycfg import prompt_loadconf
    from recert import dump,load,recert

    if (conf:=prompt_loadconf(ns.passwd)) is None:
        return 1
    seckey=conf['prikey']

    if ns.show:
        return show(load(seckey))

    if not ns.hostname:
        print('HOSTNAME is required.')
        return 1

    if ns.test:
        matchname,tlsname,fingerprint,cert=recert(ns.hostname,seckey)
        if matchname is None:
            print(f'no match for {ns.hostname}')
            return
        print(f'test hostname:               {ns.hostname}')
        print(f'match rule:                  {matchname}')
        if tlsname=='':
            tlsname='(server name verify disabled)'
        print('no tlsname override' if tlsname is None else
              f'use tlsname:                 {tlsname}')
        print('no certificate override' if fingerprint is None else
              f'use certificate fingerprint: {fingerprint}')
        return

    recerts=load(seckey)
    hostname=ns.hostname
    if ns.remove:
        if hostname not in recerts['rules']:
            print(f'no rule for {hostname}.')
            return 1
        tlsname,fingerprint=recerts['rules'].pop(hostname)
        clean_cert(fingerprint,recerts)
        return dump(recerts,seckey)

    tlsname=ns.tlsname
    cert=fingerprint=None
    if ns.cert:
        try:
            certfp=open(ns.cert,mode='rb')
        except Exception as e:
            print(f'failed to open {ns.cert}: {e}')
            return 1
        with certfp:
            cert=certfp.read()
        fingerprint=get_cert_fingerprint(cert)
    if ns.fingerprint is not None:
        if ns.fingerprint not in recerts['certs']:
            print(f'fingerprint not exists: {ns.fingerprint}')
            return 1
        if fingerprint is not None and ns.fingerprint!=fingerprint:
            print('cert fingerprint not match:')
            print(f'cert fingerprint:      {fingerprint}')
            print(f'fingerprint in option: {ns.fingerprint}')
            return 1
        fingerprint=ns.fingerprint
    if hostname in recerts['rules'] and fingerprint is None:
        _,old_fingerprint=recerts['rules'].pop(hostname)
        if old_fingerprint!=fingerprint:
            clean_cert(old_fingerprint,recerts)
    recerts['rules'][hostname]=tlsname,fingerprint
    if fingerprint is not None and fingerprint not in recerts['certs']:
        recerts['certs'][fingerprint]=minimize_certificate(cert.decode('utf8'))
    return dump(recerts,seckey)

def main():

    parser=ArgumentParser(prog='update_recerts')
    parser.add_argument(
        '--passwd',dest='passwd',action='store',metavar='PASSWORD',
        help='password to load config file.')
    parser.add_argument(
        '--hostname',dest='hostname',action='store',metavar='HOSTNAME',default=None,
        help='act on HOSTNAME.')
    parser.add_argument(
        '--tlsname',dest='tlsname',action='store',metavar='TLSNAME',default=None,
        help='set server name verify of HOSTNAME to TLSNAME, '
        'disable different tlsname if not provided, '
        'empty to disable server name verify.')
    parser.add_argument(
        '--fingerprint',dest='fingerprint',action='store',metavar='CERT',default=None,
        help='set cert fingerprint of HOSTNAME, use default cert if not provided.')
    parser.add_argument(
        '--cert',dest='cert',action='store',metavar='CERT',default=None,
        help='set cert of HOSTNAME, use default cert if not provided')
    parser.add_argument(
        '--remove',dest='remove',action='store_true',
        help='remove all rule of HOSTNAME, ignore TLSNAME and CERT options.')
    parser.add_argument(
        '--test',dest='test',action='store_true',
        help='test HOSTNAME matching, ignore TLSNAME and CERT options.')
    parser.add_argument(
        '--show',dest='show',action='store_true',
        help='show existing rules, ignore HOSTNAME, TLSNAME and CERT options.')

    return run(parser.parse_args())

if __name__=='__main__':
    exit(main())

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
