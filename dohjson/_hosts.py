#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from ipaddress import ip_address

from ._module_dir import MODULE_DIR

HOSTS=MODULE_DIR/'hosts'

HOST_TABLE={4:{},6:{},'ts':0}

def resolve_from_hosts(name,code):
    if not HOSTS.exists():
        return []
    if (ts:=HOSTS.stat().st_mtime_ns)!=HOST_TABLE['ts']:
        HOST_TABLE[4].clear()
        HOST_TABLE[6].clear()
        HOST_TABLE['ts']=ts
        # last to first
        for line in reversed(HOSTS.read_bytes().splitlines()):
            line=line.partition(b'#')[0].strip()
            if not line:
                continue
            _ip,*names=filter(None,line.decode('utf8').split())
            if not names:
                continue
            try:
                ip=ip_address(_ip)
            except:
                continue
            for n in names:
                if not n.endswith('.'):n+='.'
                HOST_TABLE[ip.version].setdefault(n,[]).append(str(ip))
    try:
        return HOST_TABLE[code][name]
    except:
        return []

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
