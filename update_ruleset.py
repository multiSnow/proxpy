#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from argparse import ArgumentParser

from proxpycfg import prompt_loadconf
from redirector import update

def main():
    parser=ArgumentParser(prog='update_ruleset')
    parser.add_argument(
        '-P',dest='passwd',action='store',metavar='PASSWORD',
        help='password to load config file.')
    if (conf:=prompt_loadconf(parser.parse_args().passwd)) is None:
        return 1
    update(conf['prikey'])

if __name__=='__main__':
    exit(main())


# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
