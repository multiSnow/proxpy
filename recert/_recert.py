#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from _thread import allocate_lock
from pickle import HIGHEST_PROTOCOL,dumps,loads

from ._module_dir import MODULE_DIR

import crypto

RECERTS_PATH=MODULE_DIR/'recerts.epickle'

RECERTS={'rules':{},'certs':{},'ts':0}
RECERTS_LOCK=allocate_lock()

def dump(recerts,seckey=None):
    assert seckey is not None
    data=dumps(recerts,protocol=HIGHEST_PROTOCOL)
    try:
        RECERTS_PATH.write_bytes(crypto.encrypt0(data,crypto.passphrase(seckey)))
    except Exception as e:
        print(f'failed to dump recerts: {e}')

def load(seckey=None):
    assert seckey is not None
    if not RECERTS_PATH.exists():
        return {'rules':{},'certs':{}}
    try:
        data=crypto.decrypt0(RECERTS_PATH.read_bytes(),crypto.passphrase(seckey))
        return loads(data)
    except Exception as e:
        print(f'failed to load recerts: {e}')

def recert(hostname,seckey=None):
    if not RECERTS_PATH.exists():
        return None,None,None,None
    with RECERTS_LOCK:
        if (ts:=RECERTS_PATH.stat().st_mtime_ns)!=RECERTS['ts']:
            assert seckey is not None
            # reload recerts.epickle
            RECERTS.clear()
            RECERTS.update(load(seckey=seckey))
            RECERTS['ts']=ts
    tlsname=fingerprint=None
    while hostname:
        if hostname in RECERTS['rules']:
            tlsname,fingerprint=RECERTS['rules'][hostname]
            break
        elif hostname.startswith('.'):
            hostname=hostname[1:]
            _,_,hostname=hostname.partition('.')
        else:
            hostname=f'.{hostname}'
    return hostname or None,tlsname,fingerprint,RECERTS['certs'].get(fingerprint,None)

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
