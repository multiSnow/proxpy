#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from _thread import allocate_lock
from functools import partial
from importlib import import_module
from io import BytesIO,FileIO
from json import dumps,loads
from pathlib import Path,PurePosixPath
from posixpath import normpath
from time import gmtime,strftime,time
from urllib.parse import urlparse

from ._http_connection import HTTPConnection,parse_headers
from ._module_dir import MODULE_DIR

DATA_FMT='%a, %d %b %Y %H:%M:%S GMT'

# cache limit size
CACHE_LIMIT=4*1024*1024

import gzip
import zlib

zlib.compress=partial(zlib.compress,level=9)

def tryimport(name):
    try:
        return import_module(name)
    except ModuleNotFoundError:
        return None

for name in ('brotliffi','brotli'):
    if brotli:=tryimport(name):
        break

for name,key in (('pyzstd','level_or_option'),
                 ('zstandard','level'),
                 ('zstd','level')):
    if zstd:=tryimport(name):
        zstd.compress=partial(zstd.compress,**{key:22})
        break

class Compresser:
    def __init__(self):
        self._fmts={}
        self._alias={}

        self._alias['x-gzip']='gzip'
        self._fmts['gzip']=gzip
        self._fmts['deflate']=zlib
        if brotli is not None:
            self._fmts['br']=brotli
        if zstd is not None:
            self._fmts['zstd']=zstd

    def __contains__(self,name):
        return name in self._fmts or name in self._alias

    @property
    def supported_formats(self):
        return set(key for key in self._fmts)

    def get_compresser(self,name):
        return self._fmts.get(self._alias.get(name,name),None)

    def compress(self,data,name):
        return self.get_compresser(name).compress(data)

    def decompress(self,data,name):
        return self.get_compresser(name).decompress(data)

class NamedLock:
    def __init__(self,name,parent=None):
        assert isinstance(parent,LockPool)
        self._lock=allocate_lock()
        self._self_lock=allocate_lock()
        self._name=name
        self._parent=parent
        self._counter=0

    @property
    def name(self):
        return self._name

    @property
    def parent(self):
        return self._parent

    def _test_purge(self):
        self._counter-=1
        if self.locked():return
        self.parent.purge(self._name)

    def acquire(self,*args,**kwds):
        assert self.name in self.parent
        with self._self_lock:
            self._counter+=1
        if rc:=self._lock.acquire(*args,**kwds):
            return rc
        with self._self_lock:
            self._test_purge()

    def release(self):
        assert self.name in self.parent
        with self._self_lock:
            assert self._lock.locked()
            self._lock.release()
            self._test_purge()

    def locked(self):
        return self._counter>0

    def __enter__(self):
        return self.acquire()

    def __exit__(self,t,v,tb):
        return self.release()

class LockPool:
    def __init__(self):
        self._locks={}
        self._self_lock=allocate_lock()

    def __call__(self,name):
        with self._self_lock:
            if name not in self._locks:
                self._locks[name]=NamedLock(name,parent=self)
            return self._locks[name]

    def __contains__(self,key):
        return key in self._locks

    def purge(self,name):
        with self._self_lock:
            assert name in self._locks
            lock=self._locks[name]
            if lock.locked():return
            del self._locks[name]

# global cache lock
_GLOBAL_CACHE_LOCK=LockPool()

# compresser
_COMPRESSER=Compresser()

def now():
    return strftime('%FT%TZ',gmtime(time()))

def dump_headers(headers):
    lines=[]
    for k,v in headers.items():
        if isinstance(v,str):
            for line in v.splitlines():
                lines.append(f'{k}: {line}')
        else:
            lines.append(f'{k}: {v}')
    lines.append('')
    try:return '\n'.join(lines)
    finally:lines.clear()

class CacheWrap:

    _accepted_headers=(
        'access-control-allow-origin',
        'content-disposition',
        'content-encoding',
        'content-type',
        'last-modified',
    )

    _preferred_compress_methods=('gzip',)

    def __init__(self,*args,expire=None,cachedir=None,cacheroot=MODULE_DIR,
                 backend=None,logger=None,**kwds):
        assert backend is not None
        self._backend=backend(*args,**kwds)
        if callable(logger):
            self._logger=logger
        else:
            self._logger=lambda *a,**k:None
        # seconds to expired, or None for live forever
        self._expire=expire or None
        if cachedir is None:
            self._cachedir=None
        else:
            self._cachedir=cacheroot/cachedir
        self._status=0
        self._response_headers={}
        self._dataio=None

    @property
    def default_headers(self):
        return self._backend.default_headers

    @property
    def headers(self):
        return self._backend.headers

    @property
    def remote_site(self):
        return self._backend.remote_site

    @property
    def closed(self):
        return self._backend.closed

    @property
    def uid(self):
        return self._backend.uid

    @property
    def errmsg(self):
        return self._backend.errmsg

    @property
    def status(self):
        if self._status>199:
            return self._status
        return self._backend.status

    def connect(self):
        pass

    def clear(self):
        self._status=0
        self._response_headers.clear()
        if self._dataio is not None:
            dataio=self._dataio
            self._dataio=None
            dataio.close()

    def close(self):
        self._backend.close()
        self.clear()

    def _get_cache(self,data_path,info_path,since):
        # return (headers,data) if cached data is responsed, else None
        if not data_path.is_file():return
        mtime=data_path.stat().st_mtime
        if self._expire and time()-mtime>self._expire:return
        headers={}
        try:
            headers.update(parse_headers(info_path.read_text(encoding='utf8')))
        except Exception as e:
            self._logger(f'cache info failed ({e}): {info_path}',self.uid)
            return
        headers.update([
            ('date',strftime(DATA_FMT,gmtime(time()))),
            ('cache-control','public, max-age=604800, immutable'),
        ])
        if since is not None and since==headers.get('last-modified'):
            for key in list(headers.keys()):
                # remove any header about content
                if key.startswith('content-'):
                    del headers[key]
            return headers,None
        try:
            data=data_path.read_bytes()
        except:
            self._logger(f'cache data failed ({e}): {data_path}',self.uid)
            return
        return headers,data

    def _accept_header(self,header):
        return header in self._accepted_headers

    @property
    def response_headers(self):
        if self._response_headers:
            return self._response_headers
        return self._backend.response_headers

    def response_iter(self,blocksize=4096):
        if self._dataio is None:
            yield from self._backend.response_iter(blocksize=blocksize)
            return
        while block:=self._dataio.read(blocksize):
            yield block

    def get_backend_api(self,apiname):
        return getattr(self._backend,apiname)

    def _request_cache(self,method,path,/,*,body=None,headers={},**kwds):
        cpath=normpath(urlparse(path).path)
        relpath=(PurePosixPath('/')/cpath).relative_to('/')
        data_path=self._cachedir/'data'/self.remote_site/relpath
        info_path=self._cachedir/'info'/self.remote_site/relpath
        info_path=info_path.with_name(info_path.name+'.txt')
        since=headers.get('if-modified-since')
        with _GLOBAL_CACHE_LOCK(data_path):

            compress_methods=[]
            choose_compress_methods=set()
            if (accept_encoding:=headers.get('accept-encoding',None)) is not None:
                for s in accept_encoding.split(','):
                    if (s:=s.strip()) and (m:=s.split(';',0)[0]) in _COMPRESSER:
                        compress_methods.append(s)
                        if m in self._preferred_compress_methods:
                            choose_compress_methods.add(m)
                if compress_methods:
                    headers['accept-encoding']=', '.join(compress_methods)
                elif 'accept-encoding' in headers:
                    # no compress encoding supported
                    del headers['accept-encoding']

            if (cache:=self._get_cache(data_path,info_path,since)) is not None:
                rheaders,data=cache
                if data is None:
                    self._status=304
                    self._dataio=BytesIO()
                else:
                    self._status=200
                    if 'content-encoding' not in rheaders:
                        names=choose_compress_methods&_COMPRESSER.supported_formats
                        for name in self._preferred_compress_methods:
                            if name not in names:continue
                            compressed_data=_COMPRESSER.compress(data,name)
                            if len(data)>len(compressed_data):
                                data=compressed_data
                                rheaders['content-encoding']=name
                                break
                    rheaders['content-length']=len(data)
                    self._dataio=BytesIO(data)
                self._response_headers.clear()
                self._response_headers.update(rheaders)
                return self._logger(f'cache wrap read: {self.remote_site}{path}',self.uid)

            self.clear()
            self._backend.request(method,path,body=body,headers=headers,**kwds)

            if self.status!=200:
                # only cache 200 status
                return self._logger(f'cache wrap skip ({self.status}): '
                                    f'{self.remote_site}{path}',self.uid)
            if 'no-store' in self.response_headers.get('cache-control',''):
                # no cache for no-store
                return self._logger(f'cache wrap skip (no-store): '
                                    f'{self.remote_site}{path}',self.uid)
            cache_control=self.response_headers.get('cache-control','')
            if self.response_headers.get('transfer-encoding','')=='chunked' and \
               ('public' not in cache_control and 'max-age' not in cache_control):
                # no cache for chunked transfer
                return self._logger(f'cache wrap skip (chunked): '
                                    f'{self.remote_site}{path}',self.uid)
            else:
                self.response_headers.pop('transfer-encoding','')
            try:
                size=int(self.response_headers.get('content-length','0'))
            except:
                size=0
            if size>CACHE_LIMIT:
                # no cache for response size over 4M
                size/=1024.0**2
                return self._logger(f'cache wrap skip (size {size:.3f}MiB): '
                                    f'{self.remote_site}{path}',self.uid)
            buflist=[]
            interrupt=False
            try:
                for b in self.response_iter():
                    buflist.append(b)
            except Exception as e:
                self._logger(f'cache wrap failed ({e}): '
                             f'{self.remote_site}{path}',self.uid)
                interrupt=True

            raw=data=b''.join(buflist)
            if interrupt:
                # incomplete data, no cache
                self._dataio=BytesIO(data)
                return
            rheaders={k:v for k,v in self.response_headers.items()
                      if self._accept_header(k)}
            if (encoding:=rheaders.get('content-encoding',None)) in _COMPRESSER:
                raw=_COMPRESSER.decompress(data,encoding)
                del rheaders['content-encoding']
                self._response_headers['content-encoding']=encoding

            self._dataio=BytesIO(data)
            self._response_headers['content-length']=len(data)
            self._response_headers.update(rheaders)
            data_path.parent.mkdir(parents=True,exist_ok=True)
            data_path.write_bytes(raw)
            info_path.parent.mkdir(parents=True,exist_ok=True)
            info_path.write_text(dump_headers(rheaders),encoding='utf8')
        return self._logger(f'cache wrap write: {self.remote_site}{path}',self.uid)

    def request(self,method,path,/,*,body=None,headers={},**kwds):
        headers={k.lower():v for k,v in headers.items()}
        try_cache=all((self._cachedir is not None,not path.endswith('/'),
                       method=='GET','range' not in headers))
        if try_cache:
            return self._request_cache(method,path,body=body,headers=headers,**kwds)
        self._logger(f'cache wrap skip: {self.remote_site}{path}',self.uid)
        self.clear()
        return self._backend.request(method,path,body=body,headers=headers,**kwds)

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
