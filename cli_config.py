#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from argparse import ArgumentParser
from argparse import ArgumentDefaultsHelpFormatter as _HelpFormatter
from pathlib import Path

import croxy
import dohjson
import http0
import mkcert
import proxpy
import proxpycfg

MODULE_DIR=Path(__file__).resolve().parent

CMD_FUNC={
    'create':proxpycfg.create_conf,
    'setpath':proxpycfg.setpath_conf,
    'setdns':proxpycfg.setdns_conf,
    'sethttp':proxpycfg.sethttp_conf,
    'setcert':proxpycfg.setcert_conf,
    'setproxy':proxpycfg.setproxy_conf,
    'delproxy':proxpycfg.delproxy_conf,
    'remote':proxpycfg.remote_conf,
    'show':proxpycfg.show_conf,
}

class ArgFormatter(_HelpFormatter):
    def _get_help_string(self, action):
        default=action.default
        choices=action.choices
        if choices:
            action.help='{} (choose from {})'.format(
                action.help,
                ', '.join(f"'{choice}'" for choice in sorted(choices)))
        if not default:
            return action.help
        return super()._get_help_string(action)

def _add_parser(subparsers,cmd,**kwds):
    return subparsers.add_parser(cmd,formatter_class=ArgFormatter,**kwds)

def main():

    # arg parser for password, used in all commands of client side.
    passwd_parser=ArgumentParser(add_help=False)
    passwd_parser.add_argument(
        '--passwd',action='store',metavar='PASSWD',
        help='password to save config file '
        '(ignored without --chpasswd if different to --loadpasswd)')
    passwd_parser.add_argument(
        '--loadpasswd',action='store',metavar='PASSWD',
        help='password to load config file '
        '(suppress prompt, default to use the same of --pass if provided)')
    passwd_parser.add_argument(
        '--chpasswd',action='store_true',
        help='change password of config file')
    passwd_parser.add_argument(
        '--nopasswd',action='store_true',
        help='disable password protection for config file '
        '(suppress prompt, --passwd and --chpasswd option)')

    # arg parser for path of remote, used in 'setpath' command of server side.
    path_parser=ArgumentParser(add_help=False)
    path_parser.add_argument(
        '--path',action='store',metavar='PATH',default='/',
        help='server root path (absolute path)')

    # arg parser for http, used in 'sethttp' command of both sides.
    http_parser=ArgumentParser(add_help=False)
    http_parser.add_argument(
        '--cacert',action='store',metavar='CACERT',required=True,
        help='certificate used to verify https connection '
        f'(absolute path or relative path to {MODULE_DIR})')
    http_parser.add_argument(
        '--cache',action='store',metavar='CACHE',default='static',
        help='path to store http cache '
        f'(absolute path or relative path to {http0.MODULE_DIR}) '
        '(cache is always disabled in server side)')

    # arg parser for DNS over HTTPS, used in 'setdns' command of both sides.
    doh_parser=ArgumentParser(add_help=False)
    doh_parser.add_argument(
        '--ip',action='store',metavar='IP',required=True,
        help='ip address of DNS over HTTPS server')
    doh_parser.add_argument(
        '--port',action='store',default=443,metavar='PORT',type=int,
        help='port of DNS over HTTPS server')
    doh_parser.add_argument(
        '--name',action='store',metavar='NAME',required=True,
        help='name of DNS over HTTPS server')
    doh_parser.add_argument(
        '--path',action='store',default='/dns-query',metavar='PATH',
        help='path of DNS over HTTPS server')
    doh_parser.add_argument(
        '--cacert',action='store',metavar='CACERT',required=True,
        help='certificate used to verify DNS over HTTPS server '
        f'(absolute path or relative path to {dohjson.MODULE_DIR})')

    # arg parser for cert generate, used in 'setcert' command of client side.
    cert_parser=ArgumentParser(add_help=False)
    cert_parser.add_argument(
        '--keyalg',action='store',
        default=mkcert.default_params['--keyalg']['default'],metavar='CURVE',
        choices=mkcert.default_params['--keyalg']['choices'],
        help='algorithm of proxy side certificate key')
    cert_parser.add_argument(
        '--curve',action='store',
        default=mkcert.default_params['--curve']['default'],metavar='CURVE',
        choices=mkcert.default_params['--curve']['choices'],
        help='EC curve name of proxy side certificate key')
    cert_parser.add_argument(
        '--md',action='store',
        default=mkcert.default_params['--md']['default'],metavar='DIGEST',
        choices=mkcert.default_params['--md']['choices'],
        help='Digest algorithm used proxy side certificate')

    # arg parser for ca cert generate, used in 'setcert' command of client side.
    ca_parser=ArgumentParser(add_help=False)
    ca_parser.add_argument(
        '--ca-name',action='store',default='Proxpy',metavar='NAME',
        help='commonName of proxy side certificate authority')
    ca_parser.add_argument(
        '--ca-expire',action='store',metavar='DAYS',type=int,
        default=mkcert.default_params['--cadays']['default'],
        help='expire days of proxy side certificate authority')

    # arg parser for site cert generate, used in 'setcert' command of client side.
    site_parser=ArgumentParser(add_help=False)
    site_parser.add_argument(
        '--site-expire',action='store',metavar='DAYS',type=int,
        default=mkcert.default_params['--servdays']['default'],
        help='expire days of proxy side per-site certificate.')

    # arg parser for proxy add/set, used in 'setproxy' command of client side.
    setproxy_parser=ArgumentParser(add_help=False)
    setproxy_parser.add_argument(
        '--proxy-name',action='store',metavar='NAME',required=True,
        help='name of proxy to add/set')
    setproxy_parser.add_argument(
        '--proxy-type',action='store',metavar='TYPE',required=True,
        choices=proxpy.BACKEND.keys(),
        help='type of proxy to add/set')
    setproxy_parser.add_argument(
        '--proxy-ip',action='store',metavar='IP',required=True,
        help='bind ip of proxy to add/set')
    setproxy_parser.add_argument(
        '--proxy-port',action='store',metavar='PORT',type=int,required=True,
        help='bind port of proxy to add/set '
        f'(range {proxpycfg.ALLOWED_PORT_RANGE[0]}'
        f' - {proxpycfg.ALLOWED_PORT_RANGE[-1]})')

    # arg parser for proxy delete, used in 'delproxy' command of client side.
    delproxy_parser=ArgumentParser(add_help=False)
    delproxy_parser.add_argument(
        '--proxy-name',action='store',metavar='NAME',required=True,
        help='name of proxy to delete')

    # arg parser for remote, used in 'remote' command of client side.
    remote_parser=ArgumentParser(add_help=False)
    remote_parser.add_argument(
        '--remote-ip',action='store',metavar='IP',
        help='remote ip (empty to auto resolve from --remote-name)')
    remote_parser.add_argument(
        '--remote-name',action='store',metavar='NAME',required=True,
        help='remote name')
    remote_parser.add_argument(
        '--remote-port',action='store',metavar='PORT',type=int,
        help='remote port (default according to protocol)')
    remote_parser.add_argument(
        '--remote-path',action='store',metavar='PATH',default='/',
        help='remote root path')
    remote_parser.add_argument(
        '--remote-nohttps',action='store_true',
        help='use http instead of https')
    remote_parser.add_argument(
        '--remote-nosni',action='store_true',
        help='disable sni in https')
    remote_parser.add_argument(
        '--cacert',action='store',metavar='CACERT',
        help='certificate used to verify https connection '
        f'(only used in config, default to system default)')
    remote_parser.add_argument(
        '--cache',action='store',metavar='CACHE',default='static',
        help='path to store http cache '
        f'(absolute path or relative path to {croxy.MODULE_DIR})')

    # root arg parser.
    parser=ArgumentParser(prog='cli_config')

    # lv1 subparser, detect which side to config.
    sideparser=parser.add_subparsers(dest='side',metavar='SIDE',required=True)
    serverside_parser=_add_parser(sideparser,'server',
                                  help='operate on server side config.')
    clientside_parser=_add_parser(sideparser,'client',
                                  help='operate on client side config.')

    # lv2 subparser, command on server side.
    serverside_modeparser=serverside_parser.add_subparsers(
        dest='mode',metavar='MODE',required=True)
    _add_parser(serverside_modeparser,'create',
                help='create/overwrite with empty config.')
    _add_parser(serverside_modeparser,'setpath',
                help='set root path.',
                parents=[path_parser])
    _add_parser(serverside_modeparser,'setdns',
                help='set DNS over HTTPS option.',
                parents=[doh_parser])
    _add_parser(serverside_modeparser,'sethttp',
                help='set HTTP connection option.',
                parents=[http_parser])
    _add_parser(serverside_modeparser,'show',
                help='show common config value.')

    # lv2 subparser, command on client side.
    clientside_modeparser=clientside_parser.add_subparsers(
        dest='mode',metavar='MODE',required=True)
    _add_parser(clientside_modeparser,'create',
                help='create/overwrite with empty config.',
                parents=[passwd_parser])
    _add_parser(clientside_modeparser,'setdns',
                help='set DNS over HTTPS option.',
                parents=[doh_parser,passwd_parser])
    _add_parser(clientside_modeparser,'sethttp',
                help='set HTTP connection option.',
                parents=[http_parser,passwd_parser])
    _add_parser(clientside_modeparser,'setcert',
                help='set proxy side certificate option.',
                parents=[cert_parser,ca_parser,site_parser,passwd_parser])
    _add_parser(clientside_modeparser,'setproxy',
                help='add or set proxy (overwrite existing one).',
                parents=[setproxy_parser,passwd_parser])
    _add_parser(clientside_modeparser,'delproxy',
                help='delete proxy.',
                parents=[delproxy_parser,passwd_parser])
    _add_parser(clientside_modeparser,'remote',
                help='set remote server (overwrite existing one).',
                parents=[remote_parser,passwd_parser])
    _add_parser(clientside_modeparser,'show',
                help='show common config value.',
                parents=[passwd_parser])

    params=parser.parse_args()
    if params.side=='client' and not params.nopasswd:
        if params.loadpasswd is None:
            params.loadpasswd=params.passwd
    else:
        params.nopasswd=True
        params.chpasswd=True
        params.passwd=None
        if params.side=='server':
            params.loadpasswd=None

    assert params.mode in CMD_FUNC,f'unknown mode: {params.mode}'
    params.rootdir=MODULE_DIR
    return CMD_FUNC[params.mode](params)

if __name__=='__main__':
    exit(main())

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
