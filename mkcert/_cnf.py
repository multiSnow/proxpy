#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

openssl_cnf='''
HOME                   = {root}
RANDFILE               = {root}/.rnd

[req]
dir                    = {root}
database               = {root}/index.txt
email_in_dn            = no
default_md             = {md}
prompt                 = no
distinguished_name     = req_dist_name

[req_dist_name]
commonName             = {commonname}

[ca_exts]
basicConstraints       = critical,CA:TRUE,pathlen:0
keyUsage               = digitalSignature,nonRepudiation,keyCertSign,cRLSign
subjectKeyIdentifier   = hash

[srv_exts]
basicConstraints       = CA:FALSE
keyUsage               = digitalSignature
extendedKeyUsage       = serverAuth
subjectKeyIdentifier   = hash
authorityKeyIdentifier = keyid:always
subjectAltName         = {dnsname}

[req_exts]
basicConstraints       = CA:FALSE
keyUsage               = digitalSignature
extendedKeyUsage       = serverAuth
subjectKeyIdentifier   = hash

[ca]
default_ca             = mainCA

[mainCA]
dir                    = {root}
database               = {root}/index.txt
new_certs_dir          = {root}
default_days           = {days}
default_md             = {md}
email_in_dn            = no
policy                 = policy_any
serial                 = {root}/serial

[policy_any]
countryName            = optional
stateOrProvinceName    = optional
organizationName       = optional
organizationalUnitName = optional
commonName             = supplied
emailAddress           = optional
'''

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
