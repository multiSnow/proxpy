#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path
import re
from subprocess import run,DEVNULL,PIPE

from ._module_dir import MODULE_DIR

OPENSSL_BINARY=dict(
    binary=None,
    version=None,
    name=None,
    checked=False,
)

_REQUIRED_VERSION={
    'OpenSSL':(1,1),
    'LibreSSL':(0,0), # assume all version works
}

SUPPORTED_MD=set()

class OpensslFailed(Exception):
    pass

def _is_valid_md(md):
    if md.startswith('sha') and not md.startswith('shake'):
        # include sha, exclude sha1
        return md!='sha1'
    return False

def _init_supported_md():
    output=run((OPENSSL_BINARY['binary'],'help'),
               capture_output=True,text=True).stderr
    mdblock=False
    for line in output.splitlines():
        line=line.strip()
        if line.startswith('Message Digest'):
            mdblock=True
            continue
        if mdblock and not line:
            break
        if not mdblock:
            continue
        SUPPORTED_MD.update(filter(_is_valid_md,line.split()))

def _get_openssl_version():
    binary=Path('openssl')
    output=None
    try:
        output=run((binary,'version'),
                   capture_output=True,text=True,check=True).stdout
    except:
        binary=MODULE_DIR/binary
        try:
            output=run((binary,'version'),
                       capture_output=True,text=True,check=True).stdout
        except:
            binary=None
    return binary,output

def openssl_exists():
    if not OPENSSL_BINARY['checked']:
        OPENSSL_BINARY['checked']=True
        binary,output=_get_openssl_version()
        if output is None:
            return False
        match=re.match(r'^(?P<name>OpenSSL|LibreSSL) (?P<version>[.\d]+)(\w+)?',
                       output)
        if match is None:
            # unknown openssl binary
            return False
        name=match.group('name')
        version=tuple(map(int,match.group('version').split('.')))
        if _REQUIRED_VERSION[name]>version:
            # version too low
            return False
        OPENSSL_BINARY['binary']=binary
        OPENSSL_BINARY['version']=version
        OPENSSL_BINARY['name']=name
        _init_supported_md()
    return True

def openssl(command,*args):
    try:
        return run((OPENSSL_BINARY['binary'],command,*args),
                   stdout=DEVNULL,stderr=DEVNULL,check=True).returncode
    except:
        raise OpensslFailed(f'{command} {args}')

def openssl_noout(command,*args,stdin=None):
    try:
        if isinstance(stdin,bytes):
            return run((OPENSSL_BINARY['binary'],command,*args),
                       input=stdin,stdout=PIPE,stderr=DEVNULL,check=True).stdout
        else:
            return run((OPENSSL_BINARY['binary'],command,*args),
                       stdout=PIPE,stderr=DEVNULL,check=True).stdout
    except:
        raise OpensslFailed(f'{command} {args}')

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
