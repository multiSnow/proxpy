#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path

from ._openssl import SUPPORTED_MD

default_params={
    '--ca':{
        'dest':'ca','metavar':'CA',
        'action':'store','type':str,
        'help':'CA name',
    },
    '--cadir':{
        'dest':'cadir','metavar':'CADIR',
        'action':'store','type':Path,
        'help':'directory to contant ca cert files',
    },
    '--serv':{
        'dest':'serv','metavar':'SERVER',
        'action':'store','type':str,
        'help':'server name',
    },
    '--servdir':{
        'dest':'servdir','metavar':'CADIR',
        'action':'store','type':Path,
        'help':'directory to contant server cert files',
    },
    '--keyalg':{
        'dest':'keyalg','metavar':'ALG',
        'action':'store','default':'ec',
        'choices':('ec','ed25519'),
        'help':'key algorithm.',
    },
    '--curve':{
        'dest':'curve','metavar':'CURVE',
        'action':'store','default':'secp521r1',
        'choices':('secp384r1','secp521r1'),
        'help':'The EC curve to use for ec key algorithm.',
    },
    '--md':{
        'dest':'md','metavar':'MD',
        'action':'store','default':'sha256',
        'choices':SUPPORTED_MD,
        'help':'The digest algorithm used in cert.',
    },
    '--cadays':{
        'dest':'cadays','metavar':'EXPIRE',
        'action':'store','default':30,'type':int,
        'help':'expire days for ca cert',
    },
    '--servdays':{
        'dest':'servdays','metavar':'EXPIRE',
        'action':'store','default':7,'type':int,
        'help':'expire days for server cert',
    },
    '--overwrite-ca':{
        'dest':'overwrite_ca',
        'action':'store_true',
        'help':'overwrite cacert',
    },
    '--overwrite-serv':{
        'dest':'overwrite_serv',
        'action':'store_true',
        'help':'overwrite server cert',
    },
    '--serv-dns':{
        'dest':'dnslist','metavar':'DNS_NAME',
        'action':'append','default':[],
        'help':'DNS name(s}, of the server',
    },
}

class ABC:
    pass

def mkparams(*,update={}):
    params=ABC()
    for flag,kwds in default_params.items():
        try:
            key=kwds['dest']
        except KeyError:
            key=flag.lstrip('-').replace('-','_')
        try:
            value=update[key]
        except KeyError:
            value=kwds.get('default',None)
        setattr(params,key,value)
    return params

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
