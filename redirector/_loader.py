#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from _thread import allocate_lock
from ipaddress import ip_address
from pickle import HIGHEST_PROTOCOL,dumps,loads
import re
from urllib.parse import urlparse

from ._module_dir import MODULE_DIR

import crypto

RULESET_PATH=MODULE_DIR/'ruleset.pickle'
RULESET_SIG_PATH=MODULE_DIR/'ruleset.pickle.sig'
XMLDIR_PATH=MODULE_DIR/'ruleset'

RULESET={'targets':{},'rulesets':{},'ts':0}
RULESET_LOCK=allocate_lock()

def isipaddr(host):
    '''Check whether host is a valid IPv4/IPv6 address.'''
    try:
        ipaddr=ip_address(host)
    except ValueError:
        return False
    else:
        if ipaddr.version in (4,6):
            return True
        return False

def dump(ruleset,prikey=None):
    assert prikey is not None
    data=dumps(ruleset,protocol=HIGHEST_PROTOCOL)
    try:
        RULESET_PATH.write_bytes(data)
        RULESET_SIG_PATH.write_text(crypto.sign(data,prikey),encoding='utf8')
    except Exception as e:
        print(f'failed to dump ruleset: {e}')

def load(pubkey=None):
    assert pubkey is not None
    try:
        data=RULESET_PATH.read_bytes()
        if crypto.verify(data,RULESET_SIG_PATH.read_text(encoding='utf8'),pubkey):
            return loads(data)
        print('failed to verify ruleset')
    except Exception as e:
        print(f'failed to load ruleset: {e}')

def update(prikey):
    # lazy import
    from ._mkruleset import mkruleset
    if (ruleset:=mkruleset(XMLDIR_PATH)) is None:
        return print('failed to update ruleset') or 1
    dump(ruleset,prikey=prikey)

def redirect_url(url,pubkey=None):
    # redirected_url,is_implicit,ruleset_name,backend or None,None,None,None
    if not RULESET_PATH.exists():
        return None,None,None,None,[],[]
    with RULESET_LOCK:
        if (ts:=RULESET_PATH.stat().st_mtime_ns)!=RULESET['ts']:
            assert pubkey is not None
            # reload ruleset.pickle
            RULESET.clear()
            RULESET.update(load(pubkey=pubkey))
            RULESET['ts']=ts
            # also clear the regular expression cache
            re.purge()
    hostname=urlparse(url).hostname.strip('.').lower()
    names=set()
    if hostname in RULESET['targets']:
        names|=set(RULESET['targets'][hostname])
    if not isipaddr(hostname):
        # no wildcard match on ip address
        head,dot,tail=hostname.partition('.')
        if '.' in tail:
            # no match wildcard on two-level-only domain
            hostname=f'*.{tail}'
            if hostname in RULESET['targets']:
                names|=set(RULESET['targets'][hostname])
    for name in sorted(names):
        if any(re.search(pattern,url,flags=re.A|re.I)
               for pattern in RULESET['rulesets'][name]['exclusion']):
            continue
        for from_pattern,to_pattern,implicit,backend,*override in \
            RULESET['rulesets'][name]['rules']:
            if re.search(from_pattern,url,flags=re.A|re.I) is None:
                continue
            if not override:
                override=[],[]
            if to_pattern is not None:
                # return url unchanged if no to_pattern in rule
                url=re.sub(from_pattern,to_pattern,url,flags=re.A|re.I)
            return url,implicit is not None,name,backend,*override
    return None,None,None,None,[],[]

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
