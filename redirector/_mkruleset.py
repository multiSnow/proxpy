#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from xml.etree import ElementTree as et

def _addxmlrule(path,*,data={}):
    root=et.parse(path).getroot()
    name=root.get('name')
    off=root.get('default_off')
    if name is None:
        return print(f'ruleset with no name is ignored: {path}')
    if off is not None:
        return print(f'default off ruleset is ignored: {name} {off}')
    if name in data['rulesets']:
        return print(f'duplicate ruleset name: {name} (in {path})') or 1
    for target in root.iter(tag='target'):
        host=target.get('host')
        if host not in data['targets']:
            data['targets'][host]=[]
        data['targets'][host].append(name)
    data['rulesets'][name]={
        'exclusion':[exclusion.get('pattern')
                     for exclusion in root.iter(tag='exclusion')],
        'rules':[(rule.get('from'),rule.get('to',default=None),
                  rule.get('implicit',default=None),
                  rule.get('backend',default=None),
                  [(request.get('header'),request.get('value',default=None))
                   for request in rule.iter(tag='request')],
                  [(response.get('header'),response.get('value',default=None))
                   for response in rule.iter(tag='response')],
                  ) for rule in root.iter(tag='rule')],
    }

def mkruleset(path):
    ruleset={'targets':{},'rulesets':{}}
    for xml in path.glob('*.xml'):
        if _addxmlrule(xml,data=ruleset):
            return
    return ruleset

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
