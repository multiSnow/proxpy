#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

import _thread
from functools import partial
from itertools import chain
from json import dumps,loads
from pathlib import Path,PurePosixPath
from ssl import create_default_context
import struct
from time import gmtime,strftime,time

import uwsgi

import crypto
import pages
from certfilter import certfilter
from dohjson import DNSClient
from http0 import HTTPConnection
from proxpycfg import loadconf
from streampacker import stream_iter_pack

TOKEN_HEADER_FMT='<LL'

class ABC:
    def __init__(self,**kwds):
        for k,v in kwds.items():
            setattr(self,k,v)

CPOOL=ABC(
    dnsclient=None,
    http={},
    lock=_thread.allocate_lock(),
)

def now():
    return strftime('%FT%TZ',gmtime(time()))

def clean_pool(timeout=60):
    cur=time()
    for uid in set(CPOOL.http.keys()):
        if (duration:=cur-CPOOL.http[uid][2])<timeout:
            continue
        lock,conn,epoch=CPOOL.http.pop(uid)
        with lock:
            conn.close()
        print(f'{now()} (cleanup) uid:{uid} timeout:{int(duration)}')
    return

def _unpack_token(token):
    hdrlen=struct.calcsize(TOKEN_HEADER_FMT)
    keylen,siglen=struct.unpack(TOKEN_HEADER_FMT,token[:hdrlen])
    data=token[hdrlen:]
    if keylen+siglen!=len(data):
        return None,None
    return data[:keylen],data[keylen:]

def reqinfo(wsgienv):
    addr=wsgienv.get('HTTP_X_FORWARDED_FOR',None)
    addr=addr or wsgienv.get('REMOTE_ADDR',None)
    ua=wsgienv.get('HTTP_USER_AGENT',None) or None
    method=wsgienv.get('REQUEST_METHOD',None) or None
    path=wsgienv.get('PATH_INFO',None) or None
    return addr,ua,method,path

def send_chunk(data=b''):
    if isinstance(data,str):
        data=data.encode('utf8')
    yield f'{len(data):x}\r\n'.encode('utf8')
    yield data
    yield b'\r\n'

def iterchunk(*,prikey=None,pubkey=None,msg={}):
    if prikey and pubkey:
        decryptor=partial(crypto.decrypt2,prikey=prikey,pubkey=pubkey)
    else:
        decryptor=lambda b:b
    msg['len']=0
    msg['count']=0
    try:
        while len(b:=uwsgi.chunked_read())>0:
            yield (d:=decryptor(b))
            msg['len']+=len(d)
            msg['count']+=1
    except Exception as e:
        msg['ok']='FAILED'
    else:
        msg['ok']='OK'

def nothing(wsgienv,set_status):
    set_status('403',[('content-length','0'),
                      ('connection','close')])
    yield b''

def notfound(wsgienv,set_status):
    set_status('404',[
        ('content-length',str(len(pages.PAGE_404))),
        ('content-type','text/html; charset=utf-8'),
        ('cache-control','no-cache, no-store'),
    ])
    addr,ua,method,path=reqinfo(wsgienv)
    print(f'{now()} (refused) addr:{addr} ua:{ua} {method}:{path}')
    yield pages.PAGE_404

def show_pubkey(wsgienv,set_status,conf=None):
    pubkey=crypto.generate_pubkey(conf['prikey'])
    set_status('200',[('content-type','application/octet-stream'),
                      ('content-length',str(len(pubkey)))])
    addr,ua,method,path=reqinfo(wsgienv)
    print(f'{now()} (pubkey) addr:{addr} ua:{ua}')
    yield pubkey

def get_sign(wsgienv,set_status,conf=None):
    if wsgienv['REQUEST_METHOD']!='POST':
        yield from notfound(wsgienv,set_status)
        return
    if 'CONTENT_LENGTH' not in wsgienv:
        yield from notfound(wsgienv,set_status)
        return
    try:
        length=int(wsgienv['CONTENT_LENGTH'])
    except:
        yield from notfound(wsgienv,set_status)
        return
    if length>4096:
        yield from notfound(wsgienv,set_status)
        return
    data=wsgienv['wsgi.input'].read(length)
    try:
        client_pubkey=crypto.decrypt1(data,conf['prikey'])
    except:
        yield from notfound(wsgienv,set_status)
        return
    signature=bytes.fromhex(crypto.sign(client_pubkey,conf['prikey']))
    set_status('200',[('content-type','application/octet-stream'),
                      ('content-length',str(len(signature)))])
    addr,ua,method,path=reqinfo(wsgienv)
    print(f'{now()} (do sign) addr:{addr} ua:{ua} pubkey:{client_pubkey.hex()}')
    yield signature

def getpysrc(wsgienv,set_status,conf=None):
    import gzip
    import certfilter,crypto,pages,dohjson,http0,proxpycfg,streampacker
    addr,ua,method,path=reqinfo(wsgienv)
    buf=[]
    errbuf=[]
    for module in (certfilter,crypto,pages,dohjson,
                   http0,proxpycfg,streampacker):
        try:
            buf.append(getattr(module,'getpysrc')())
        except Exception as e:
            errbuf.append(f'failed to get source of {module.__name__}: {e}')
    me=Path(__file__)
    lic=me.with_name('LICENSE')
    for p in (me,lic):
        try:
            with p.open(mode='rt',encoding='utf8') as f:
                buf.append(f'{p.name}\n\n'+''.join(f'## {line}' for line in f))
        except Exception as e:
            errbuf.append(f'failed to get source of {p.name}: {e}')

    data='\n\n'.join(errbuf if errbuf else buf)
    signature=crypto.sign(data.encode('utf8'),conf['prikey'])

    data=f'{signature}\n{data}'.encode('utf8')
    headers=[('content-type','text/plain'),('transfer-encoding','chunked')]
    if 'gzip' in wsgienv.get('HTTP_ACCEPT_ENCODING','').lower():
        data=gzip.compress(data,compresslevel=9,mtime=0)
        headers.append(('content-encoding','gzip'))

    set_status('200',headers)
    yield from send_chunk(data)
    yield from send_chunk()
    print(f'{now()} (source) addr:{addr} ua:{ua}')

def reserver(wsgienv,set_status,conf=None):
    addr,ua,method,path=reqinfo(wsgienv)
    if not (b:=uwsgi.chunked_read()):
        yield from notfound(wsgienv,set_status)
        print(f'{now()} (no auth) addr:{addr} ua:{ua}')
        return
    # first chunk: auth
    client_pubkey,signature=_unpack_token(crypto.decrypt1(b,conf['prikey']))
    if not crypto.verify(
            client_pubkey,signature.hex(),
            crypto.generate_pubkey(conf['prikey'])):
        yield from notfound(wsgienv,set_status)
        print(f'{now()} (autherr) addr:{addr} ua:{ua}')
        return

    if not (b:=uwsgi.chunked_read()):
        yield from notfound(wsgienv,set_status)
        print(f'{now()} (no data) addr:{addr} ua:{ua}')
        return
    # second chunk: urldata or 'close'
    urldata=crypto.decrypt2(b,conf['prikey'],client_pubkey)
    if urldata.startswith(b'close'):
        while uwsgi.chunked_read():pass
        uid=urldata[5:].decode('utf8')
        lock=None
        with CPOOL.lock:
            if uid in CPOOL.http:
                print(f'{now()} ( close ) addr:{addr} uid:{uid}')
                lock,conn,epoch=CPOOL.http.pop(uid)
        if lock is None:
            print(f'{now()} ( close ) addr:{addr} uid:{uid}(invalid)')
        else:
            with lock:
                conn.close()
        yield from nothing(wsgienv,set_status)
        return
    method,scheme,hostname,port,path,query,uid=loads(urldata)

    if not (b:=uwsgi.chunked_read()):
        yield from notfound(wsgienv,set_status)
        print(f'{now()} (no header) addr:{addr} ua:{ua}')
        return
    # third chunk: headers
    headers=loads(crypto.decrypt2(b,conf['prikey'],client_pubkey))
    chunkmsg={}
    if method=='POST':
        chunks=iterchunk(prikey=conf['prikey'],pubkey=client_pubkey,msg=chunkmsg)
    else:
        chunks=None

    if scheme=='https':
        cacert=Path(__file__).resolve().parent/conf['cacert']
        ctx=create_default_context(cafile=cacert)
        ctx.set_alpn_protocols(['http/1.1'])
    else:
        ctx=None
    chunked_header=headers.get('transfer-encoding','').lower()=='chunked'

    with CPOOL.lock:
        clean_pool()
        if CPOOL.dnsclient is None:
            CPOOL.dnsclient=DNSClient(
                dns_ip=conf['doh']['ip'],dns_port=conf['doh']['port'],
                dns_name=conf['doh']['name'],dns_path=conf['doh']['path'],
                cacert=conf['doh']['cacert'])
            print(f'{now()} (new dns) addr:{addr}')
        if uid not in CPOOL.http:
            conn=HTTPConnection(
                hostname,port=port,context=ctx,
                hostname=hostname,tlsname=hostname,
                dnsclient=CPOOL.dnsclient,certfilter=certfilter)
            CPOOL.http[uid]=[_thread.allocate_lock(),conn,0]
            print(f'{now()} (connect) addr:{addr} uid:{uid} '
                  f'{method}:{scheme}:{hostname}{path}')
        CPOOL.http[uid][2]=time()

    lock,conn,epoch=CPOOL.http[uid]
    with lock:
        set_status('200',[('transfer-encoding','chunked')])
        print(f'{now()} (request) addr:{addr} uid:{uid} '
              f'{method}:{scheme}:{hostname}{path}')
        conn.request(method,f'{path}?{query}' if query else path,
                     body=chunks,headers=headers,
                     encode_chunked=chunked_header)
        if chunkmsg:
            print(f'{now()} ( chunk ) addr:{addr} uid:{uid} '
                  f'{method}:{scheme}:{hostname}{path} '
                  '{m[ok]}: {m[count]} ({m[len]} bytes)'.format(m=chunkmsg))
        encryptor=partial(crypto.encrypt2,
                          prikey=conf['prikey'],pubkey=client_pubkey)
        headerjson=dumps(conn.response_headers,separators=(',',':'))
        stat=f'{conn.status}'.encode('utf8')
        hblk=headerjson.encode('utf8')
        if conn.status>599:
            # internal error
            data=conn.response_io.read()
            print(f'{now()} (reserve) addr:{addr} uid:{uid} '
                  f'{method}:{scheme}:{hostname}{path} {conn.status}',
                  data.decode('utf8'))
            for i,eblock in enumerate(stream_iter_pack(
                    (stat,hblk,data),packer=encryptor),1):
                yield from send_chunk(eblock)
        else:
            print(f'{now()} (reserve) addr:{addr} uid:{uid} '
                  f'{method}:{scheme}:{hostname}{path} {conn.status}')
            for i,eblock in enumerate(stream_iter_pack(
                    chain((stat,hblk),conn.response_iter()),
                    packer=encryptor),1):
                yield from send_chunk(eblock)
    yield from send_chunk()
    print(f'{now()} (reserve) addr:{addr} uid:{uid} '
          f'{method}:{scheme}:{hostname}{path} end ({i} chunks)')

def application(wsgienv,set_status):
    conf=loadconf('server')
    rpath=PurePosixPath(conf['path'])
    FUNC_MAP={
        (rpath/'pubkey').as_posix():show_pubkey,
        (rpath/'sign').as_posix():get_sign,
        (rpath/'getpysrc').as_posix():getpysrc,
    }
    if (path:=wsgienv.get('PATH_INFO',None)) in FUNC_MAP:
        yield from FUNC_MAP[path](wsgienv,set_status,conf=conf)
        return
    if path is None:
        yield from nothing(wsgienv,set_status)
        return
    if wsgienv.get('REQUEST_METHOD','')!='POST' or \
       'CONTENT_LENGTH' in wsgienv or \
       wsgienv.get('HTTP_TRANSFER_ENCODING','').lower()!='chunked':
        yield from notfound(wsgienv,set_status)
        return

    yield from reserver(wsgienv,set_status,conf=conf)

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
