#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

import json

import crypto

class PasswordRequired(Exception):
    pass

class nonedict(dict):
    '''Automatically discard item with None value.'''
    def __getitem__(self,key):
        if key not in self:
            return None
        if (val:=super().__getitem__(key)) is None:
            super().__delitem__(key)
            return None
        return val
    def __setitem__(self,key,val):
        if val is None:
            if key in self:
                super().__delitem__(key)
            return None
        super().__setitem__(key,val)
    def __repr__(self):
        return f'{self.__class__.__name__}({super().__repr__()})'

def _dumpjson(obj,path,/,*,passwd=None):
    assert passwd is None or isinstance(passwd,str),f'passwd type: {type(passwd)}'
    encrypted=0
    data=json.dumps(obj,separators=(',', ':'),sort_keys=True).encode('utf8')
    if passwd is not None:
        encrypted=1
        data=crypto.encrypt0(data,crypto.passphrase(passwd))
    path.write_bytes(encrypted.to_bytes(1,byteorder='big')+data)
    return True

def _loadjson(path,/,*,passwd=None):
    assert passwd is None or isinstance(passwd,str),f'passwd type: {type(passwd)}'
    data=path.read_bytes()
    encrypted=data[0]
    data=data[1:]
    if encrypted:
        if passwd is None:
            raise PasswordRequired('password required')
        data=crypto.decrypt0(data,crypto.passphrase(passwd))
    return json.loads(data,object_hook=nonedict)

def _saveconf(name,conf,/,*,passwd=None,module_dir=None):
    try:
        return _dumpjson(conf,module_dir/f'{name}.json',passwd=passwd)
    except Exception as e:
        print(f'failed to dump {name} side config: {e}.')

def _loadconf(name,/,*,passwd=None,module_dir=None):
    try:
        return _loadjson(module_dir/f'{name}.json',passwd=passwd)
    except PasswordRequired as e:
        raise e
    except Exception as e:
        print(f'failed to load {name} side config: {e}.')

def initconf(*,serverside=False):
    conf=nonedict(
        prikey=crypto.generate_prikey(as_hex=True),
    )
    conf['doh']=nonedict()
    if serverside:
        conf['path']='/'
    else:
        conf['proxy']=nonedict()
        conf['proxy']['servers']=nonedict()
        conf['remote']=nonedict()
    return conf

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
