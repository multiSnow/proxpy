#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

HTTP_FMT='''[http]
ca certificate    = {c[cacert]} (absolute or relative to {r})
offline cache     = {c[cache]} (absolute or relative to {d})
'''

SERV_FMT='''[server]
server root path  = {c[path]}
'''

DOH_FMT='''[dns]
ip address        = {c[doh][ip]}
DoH port          = {c[doh][port]}
dns hostname      = {c[doh][name]}
json query path   = {c[doh][path]}
ca certificate    = {c[doh][cacert]} (absolute or relative to {d})
'''

CERT_FMT='''[cert]
key algorithm     = {c[proxy][keyalg]}
curve name of ecc = {c[proxy][curve]}
message digest    = {c[proxy][md]}
ca name           = {c[proxy][ca_name]} (save cert to {crt})
ca expire days    = {c[proxy][ca_expire]}
site expire days  = {c[proxy][site_expire]}
'''

PROXY_FMT='''[proxy ({0})]
type:ip:port      = {1[0]}:{1[1]}:{1[2]}
'''

REMOTE_FMT='''[remote]
remote ip         = {c[remote][ip]}
remote hostname   = {c[remote][name]}
remote port       = {c[remote][port]}
remote root path  = {c[remote][path]}
remote use https  = {c[remote][https]}
remote need sni   = {c[remote][sni]}
offline cache     = {c[remote][cache]} (absolute or relative to {d})
'''

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
