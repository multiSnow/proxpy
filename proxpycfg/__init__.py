#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

from ._module_dir import MODULE_DIR,getpysrc
from ._command import ALLOWED_PORT_RANGE
from ._command import nonedict,loadconf,prompt_loadconf
from ._command import create_conf,setpath_conf,show_conf
from ._command import setdns_conf,sethttp_conf
from ._command import setcert_conf,remote_conf
from ._command import setproxy_conf,delproxy_conf

if __name__=='__main__':
    exit(0)

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
