#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

import functools
import multiprocessing.dummy as mt
import threading

class OrderedQueue:
    class Dup(Exception):pass
    class Closed(Exception):pass
    class RaceGet(Exception):pass
    class NotPrepared(Exception):pass

    def __init__(self):
        self._get_lock=threading.Lock()
        self._lock=threading.Lock()
        self._condition=threading.Condition()
        self._closed=False
        self._buf={}
        self._current=0

    def __iter__(self):
        while True:
            try:
                yield self.get()
            except self.Closed:
                break

    def _check_ready(self):
        with self._lock:
            return self._buf.get(self._current,{'ready':False})['ready']

    def _get(self):
        with self._condition:
            self._condition.wait_for(self._check_ready)
        with self._lock:
            item=self._buf.pop(self._current).pop('item')
            self._current+=1
            return item

    def prepare(self,index):
        with self._lock:
            if self.closed:
                raise self.Closed
            if index in self._buf or index<self._current:
                raise self.Dup(index)
            self._buf[index]={'ready':False,'item':None}

    def ready(self,index,item):
        with self._condition,self._lock:
            if index not in self._buf:
                raise self.NotPrepared(index)
            self._buf[index]['item']=item
            self._buf[index]['ready']=True
            self._condition.notify()

    def put(self,index,item):
        self.prepare(index)
        self.ready(index,item)

    def get(self):
        if not self._get_lock.acquire(blocking=False):
            raise self.RaceGet
        try:
            if self.closed and not self._buf:
                raise self.Closed
            return self._get()
        finally:
            self._get_lock.release()

    @property
    def closed(self):
        return self._closed

    def close(self):
        with self._condition,self._lock:
            self._closed=True
            self._condition.notify()

def tpool(queue,func,iterable):
    with mt.Pool() as pool:
        for n,item in enumerate(iterable):
            queue.prepare(n)
            pool.apply_async(
                func,(item,),
                callback=functools.partial(queue.ready,n))
        queue.close()
        pool.close()
        pool.join()

def pmap(target,iterable):
    queue=OrderedQueue()
    pool_thread=threading.Thread(target=tpool,args=(queue,target,iterable))
    pool_thread.start()
    yield from queue
    pool_thread.join()

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
