#  This file is part of proxpy.
#
#  proxpy is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  proxpy is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with proxpy.  If not, see <https://www.gnu.org/licenses/>.

import functools
import hashlib
import struct

from .pmap import pmap

class StreamUnpackError(Exception):pass

BLOCK_HEADER_FMT='<LL'

def _pack_block(packer,hasher,block):
    try:
        result=packer(block) if callable(packer) else block
        digest=hasher(block) if callable(hasher) else b''
        header=struct.pack(BLOCK_HEADER_FMT,len(result),len(digest))
        return header+result+digest
    except Exception as e:
        return e

def _unpack_block(unpacker,checker,block_pair):
    try:
        block,digest=block_pair
        result=unpacker(block) if callable(unpacker) else block
        if callable(checker) and not checker(result,digest):
            raise StreamUnpackError
        return result
    except Exception as e:
        return e

def _stream_reader_dec(reader):
    hdrlen=struct.calcsize(BLOCK_HEADER_FMT)
    while len(header:=reader.read(hdrlen))==hdrlen:
        block_len,digest_len=struct.unpack(BLOCK_HEADER_FMT,header)
        yield reader.read(block_len),reader.read(digest_len)

def _reader2iter(reader,blocksize):
    while block:=reader.read(blocksize):
        yield block

def stream_pack(reader,packer=None,hasher=None,blocksize=4096,parallel=False):
    assert isinstance(blocksize,int)
    assert blocksize<2**32
    yield from stream_iter_pack(_reader2iter(reader,blocksize),
                                packer=packer,hasher=hasher,parallel=parallel)

def stream_iter_pack(iterable,packer=None,hasher=None,parallel=False):
    _map=pmap if parallel else map
    for block in _map(functools.partial(_pack_block,packer,hasher),iterable):
        if isinstance(block,Exception):
            raise block
        yield block

def stream_unpack(reader,unpacker=None,checker=None,parallel=False):
    _map=pmap if parallel else map
    for block in _map(functools.partial(_unpack_block,unpacker,checker),
                      _stream_reader_dec(reader)):
        if isinstance(block,Exception):
            raise block
        yield block

# Local Variables:
# coding: utf-8
# mode: python
# python-indent-offset: 4
# indent-tabs-mode: nil
# End:
